# [![Codacy Badge](https://api.codacy.com/project/badge/2cab839e85084141bc628b3b39276ead)](https://www.codacy.com/app/relic7/mozu_img) #

# [![Code Issues](https://www.quantifiedcode.com/api/v1/project/8bc86accb4af418fb94a9d9f7d7a48f8/badge.svg)](https://www.quantifiedcode.com/app/project/8bc86accb4af418fb94a9d9f7d7a48f8) #

# [![Coverage Status](https://coveralls.io/repos/relic7/mozu_img/badge.svg?branch=master&service=bitbucket)](https://coveralls.io/bitbucket/relic7/mozu_img?branch=master) #

# [![AppveyorCI status](https://ci.appveyor.com/api/projects/status/1i8qa6xnrj7nq3i7/branch/master?svg=true)](https://ci.appveyor.com/project/relic7/mozu-img/branch/master) #

# [![SemaphoreCI Status](https://semaphoreci.com/api/v1/projects/1f848a75-2405-4728-b109-2cd2e65ce3d5/579817/badge.svg)](https://semaphoreci.com/relic7/mozu_img)

# README #
### What is this repository for? ###

* PROD_IMAGES_DEV
* Ver 1

## Summary of set up ##
* Configuration
    ** pip install -r requirements.txt **
* Dependencies
    ** requirements.txt **
* Database configuration
    ** db.py **
* How to run tests
* Deployment instructions