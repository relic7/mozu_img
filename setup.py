#!/usr/bin/env python
# coding: utf-8

from distutils.core import setup

setup(name='mozubfly_rest_client',
      version='1.0',
      description='Image Communication and Transfer Utilities',
      author='John Bragato',
      author_email='John.Bragato@bluefly.com',
      url='https://www.python.org/relic7/mozubfly-rest-client/',
      packages=['mozu',
                'mozu.base_config',
                'mozu.RESTClient',
                'mozu.db',
                'mozu.mozu_image_util_functions',
                'mozu.mozu_exec'
                ],
      py_modules=['sqlalchemy', 'requests'], requires=['sqlalchemy', 'requests']
      )
